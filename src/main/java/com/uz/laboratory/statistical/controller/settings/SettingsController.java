package com.uz.laboratory.statistical.controller.settings;

import com.dalsemi.onewire.OneWireException;
import com.dalsemi.onewire.adapter.DSPortAdapter;
import com.dalsemi.onewire.application.tag.TaggedDevice;
import com.dalsemi.onewire.container.TemperatureContainer;
import com.uz.laboratory.statistical.app.PonabStatisticalReports;
import com.uz.laboratory.statistical.bean.adapter.GenericAdapter;
import com.uz.laboratory.statistical.bean.init.InitComboBoxesUtil;
import com.uz.laboratory.statistical.bean.init.InitTemperatureDeviceSettingsUtil;
import com.uz.laboratory.statistical.bean.movement.EvenMovement;
import com.uz.laboratory.statistical.bean.movement.UnevenMovement;
import com.uz.laboratory.statistical.bean.sensor.GlobalSensorList;
import com.uz.laboratory.statistical.bean.sensor.GlobalSensorMap;
import com.uz.laboratory.statistical.bean.sensor.OutsideTemperatureSensors;
import com.uz.laboratory.statistical.dict.Constants;
import com.uz.laboratory.statistical.entity.settings.ThermalSettings;
import com.uz.laboratory.statistical.service.thermal.ThermalSettingsService;
import com.uz.laboratory.statistical.service.util.utilImpl.HibernateUtilServiceImpl;
import com.uz.laboratory.statistical.util.GitUtil;
import com.uz.laboratory.statistical.util.fx.AlertGuiUtil;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import jssc.SerialPort;
import jssc.SerialPortList;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

@Controller
public class SettingsController implements Initializable {
    private final Object syncObj = new Object();
    @FXML
    public Button updateDatabaseButton;
    @FXML
    public Button updateCurrentDatabaseButton;
    @FXML
    public Button updateAllButton;
    @FXML
    public TextArea loggerInformation;
    @FXML
    public TextField sensorIdTextField;
    @FXML
    public ComboBox checkSensorIdComboBox;
    @FXML
    public ComboBox<String> evenLeftSensorIdComboBox;
    @FXML
    public ComboBox<String> unevenLeftSensorIdComboBox;
    @FXML
    public ComboBox<String> evenRightSensorIdComboBox;
    @FXML
    public ComboBox<String> unevenRightSensorIdComboBox;
    @FXML
    public ComboBox<String> temperatureLeftSensorIdComboBox;
    @FXML
    public ComboBox<String> temperatureRightSensorIdComboBox;
    @FXML
    public ComboBox<String> adapterPortComboBox;
    @FXML
    public ComboBox<String> adapterComboBox;
    @FXML
    public TextArea checkedSensorStatusTextArea;
    @FXML
    public ComboBox<String> unevenPortComboBox;
    @FXML
    public ComboBox<String> evenPortComboBox;
    @FXML
    public Button checkSensorByIdFromListButton;
    @FXML
    public Button checkSensorByIdButton;
    @Autowired
    public GlobalSensorList globalSensorList;
    @Autowired
    public GlobalSensorMap globalSensorMap;
    @Autowired
    private GenericAdapter genericAdapter;
    @Autowired
    private InitComboBoxesUtil initComboBoxesUtil;
    @Autowired
    private InitTemperatureDeviceSettingsUtil initTemperatureDeviceSettingsUtil;
    @Autowired
    private GitUtil gitUtil;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private HibernateUtilServiceImpl hibernateUtilService;
    @Autowired
    private ThermalSettingsService thermalSettingsService;
    @Autowired
    private EvenMovement evenMovement;
    @Autowired
    private UnevenMovement unevenMovement;
    @Autowired
    private OutsideTemperatureSensors outsideTemperatureSensors;

    @FXML
    private Service<Void> updateAllDataThreadService = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    updateAllButton.setDisable(true);
                    loggerInformation.appendText("Производиться обновление всей информации о датчиках и адаптерах... \n");
                    globalSensorMap.clear();
                    globalSensorList.clear();
                    initComboBoxesUtil.initComboBoxes();
                    initTemperatureDeviceSettingsUtil.initTemperatureDeviceAndComboBoxes();
                    updateComboBoxes();
                    updateAllButton.setDisable(false);
                    loggerInformation.appendText("Обновление успешно завершено. \n");
                    updateAllDataThreadService.cancel();
                    return null;
                }
            };
        }
    };


    @FXML
    private Service<Void> checkSensorStatusByIdThreadService = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    if (StringUtils.isNotBlank(sensorIdTextField.getText())
                            && globalSensorMap.containsKey(sensorIdTextField.getText())
                            && genericAdapter.getBaseAdapter() != null
                            && genericAdapter.getBaseAdapter().adapterDetected()) {
                        checkSensorByIdFromListButton.setDisable(true);
                        checkSensorByIdButton.setDisable(true);
                        DSPortAdapter l_adapter = null;
                        TemperatureContainer l_container = null;
                        synchronized (syncObj) {
                            l_adapter = genericAdapter.getBaseAdapter();
                            System.out.println(globalSensorMap.get(sensorIdTextField.getText()).toString());
                            l_container = (TemperatureContainer) ((TaggedDevice) globalSensorMap.get(sensorIdTextField.getText())).getDeviceContainer();
                        }
                        try {
                            l_adapter.beginExclusive(true);
                            byte[] state = l_container.readDevice();
                            System.out.println(Arrays.toString(state));
                            l_container.doTemperatureConvert(state);
                            if (StringUtils.isNotEmpty(checkedSensorStatusTextArea.getText())) {
                                checkedSensorStatusTextArea.setText("");
                            }
                            checkedSensorStatusTextArea.setText(Constants.SENSOR_CHECK_MESSAGE + Double.toString(l_container.getTemperature(state)));
                            loggerInformation.appendText("Проверен датчик: " + sensorIdTextField.getText() + "Температура: " + Double.toString(l_container.getTemperature(state)));
                        } catch (Exception e) {
                            System.out.println(Constants.ERROR_READING_DEVICE + e.toString());
                            loggerInformation.appendText(Constants.ERROR_READING_DEVICE);
                        } finally {
                            l_adapter.endExclusive();
                            checkSensorByIdFromListButton.setDisable(false);
                            checkSensorByIdButton.setDisable(false);
                        }
                    } else {
                        loggerInformation.appendText(Constants.WRONG_SENSOR_ID_INSERTED);
                        checkedSensorStatusTextArea.setText(Constants.WRONG_SENSOR_ID_INSERTED);
                    }
                    checkSensorStatusByIdThreadService.cancel();
                    return null;
                }
            };
        }
    };

    @FXML
    private Service<Void> checkSensorStatusByIdFromListThreadService = new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    if (StringUtils.isNotBlank((CharSequence) checkSensorIdComboBox.getSelectionModel().getSelectedItem())
                            && globalSensorMap.containsKey(checkSensorIdComboBox.getSelectionModel().getSelectedItem())
                            && genericAdapter.getBaseAdapter() != null
                            && genericAdapter.getBaseAdapter().adapterDetected()) {
                        checkSensorByIdFromListButton.setDisable(true);
                        checkSensorByIdButton.setDisable(true);
                        TaggedDevice taggedDevice = (TaggedDevice) globalSensorMap.get(checkSensorIdComboBox.getSelectionModel().getSelectedItem().toString());
                        DSPortAdapter l_adapter = null;
                        TemperatureContainer l_container = null;
                        synchronized (syncObj) {
                            l_adapter = genericAdapter.getBaseAdapter();
                            l_container = (TemperatureContainer) taggedDevice.getDeviceContainer();
                        }
                        try {
                            l_adapter.beginExclusive(true);
                            byte[] state = l_container.readDevice();
                            l_container.doTemperatureConvert(state);
                            if (StringUtils.isNotEmpty(checkedSensorStatusTextArea.getText())) {
                                checkedSensorStatusTextArea.setText("");
                            }
                            checkedSensorStatusTextArea.setText("Статус: Доступен;  Проверка: Исправен;  Температура: " + Double.toString(l_container.getTemperature(state)));
                            loggerInformation.appendText("Проверен датчик: " + checkSensorIdComboBox.getSelectionModel().getSelectedItem().toString() + ";  Температура: " + Double.toString(l_container.getTemperature(state)) + "\n");
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("Error reading device!" + e.toString());
                            loggerInformation.appendText("Error reading device! ! \n");
                            checkedSensorStatusTextArea.setText("Ошибка при чтении с адаптера!");
                            checkSensorByIdFromListButton.setDisable(false);
                            checkSensorByIdButton.setDisable(false);
                        } finally {
                            l_adapter.endExclusive();
                            checkSensorByIdFromListButton.setDisable(false);
                            checkSensorByIdButton.setDisable(false);
                        }
                    } else {
                        loggerInformation.appendText("Вы ввели пустое значение или такого устройства не существует в адаптере либо адаптер недоступен! \n");
                        checkedSensorStatusTextArea.setText("Вы ввели пустое значение или такого устройства не существует в адаптере либо адаптер недоступен!");
                    }
                    checkSensorStatusByIdFromListThreadService.cancel();
                    return null;
                }
            };
        }
    };

    public SettingsController() {
        checkSensorIdComboBox = new ComboBox();
        evenLeftSensorIdComboBox = checkSensorIdComboBox;
        evenRightSensorIdComboBox = checkSensorIdComboBox;
        unevenLeftSensorIdComboBox = checkSensorIdComboBox;
        unevenRightSensorIdComboBox = checkSensorIdComboBox;
        temperatureLeftSensorIdComboBox = checkSensorIdComboBox;
        temperatureRightSensorIdComboBox = checkSensorIdComboBox;
    }

    @FXML
    public void updateDatabaseButtonListener(ActionEvent actionEvent) {
        try {
            if (java.lang.Runtime.getRuntime().exec("ping www.github.com").waitFor() != 0) {
                AlertGuiUtil.createAlert(Constants.INTERNET_IS_NOT_AVAILABLE);
            } else {
                gitUtil.commitAndPush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void updateCurrentDatabaseButtonListener(ActionEvent actionEvent) {
        try {
            if (java.lang.Runtime.getRuntime().exec("ping www.github.com").waitFor() != 0) {
                AlertGuiUtil.createAlert(Constants.INTERNET_IS_NOT_AVAILABLE);
            } else {
                hibernateUtilService.shutdownDataBase();
                ((ConfigurableApplicationContext) applicationContext).close();
                gitUtil.pullAndMerge();
                PonabStatisticalReports.restartMainStage();
            }
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loggerInformation.textProperty().addListener((observable, oldValue, newValue) -> {
            loggerInformation.setScrollTop(Double.MAX_VALUE);
        });
        updateComboBoxes();
        ThermalSettings thermalSettings = (ThermalSettings) thermalSettingsService.get(1);
        if (thermalSettings != null) {
            evenPortComboBox.getSelectionModel().select(thermalSettings.getEvenControlPortAddress() != null ? thermalSettings.getEvenControlPortAddress() : "");
            unevenPortComboBox.getSelectionModel().select(thermalSettings.getUnevenControlPortAddress() != null ? thermalSettings.getUnevenControlPortAddress() : "");
            evenLeftSensorIdComboBox.getSelectionModel().select((thermalSettings.getEvenLeftIronSensorAddress() != null ? thermalSettings.getEvenLeftIronSensorAddress() : ""));
            evenRightSensorIdComboBox.getSelectionModel().select((thermalSettings.getUnevenLeftIronSensorAddress() != null ? thermalSettings.getUnevenLeftIronSensorAddress() : ""));
            unevenLeftSensorIdComboBox.getSelectionModel().select((thermalSettings.getUnevenLeftIronSensorAddress() != null ? thermalSettings.getUnevenLeftIronSensorAddress() : ""));
            unevenRightSensorIdComboBox.getSelectionModel().select(thermalSettings.getUnevenRightIronSensorAddress() != null ? thermalSettings.getUnevenRightIronSensorAddress() : "");
            temperatureLeftSensorIdComboBox.getSelectionModel().select(thermalSettings.getTemperatureLeftSensorAddress() != null ? thermalSettings.getTemperatureLeftSensorAddress() : "");
            temperatureRightSensorIdComboBox.getSelectionModel().select(thermalSettings.getTemperatureRightSensorAddress() != null ? thermalSettings.getTemperatureRightSensorAddress() : "");
            adapterComboBox.getSelectionModel().select(thermalSettings.getAdapterAddress());
            adapterPortComboBox.getSelectionModel().select(thermalSettings.getAdapterPort());
        }
    }

    private void updateComboBoxes() {
        List totalSerialPortList = Arrays.asList(SerialPortList.getPortNames());
        try {
            if (genericAdapter.getBaseAdapter() != null) {
                adapterComboBox.getItems().addAll(genericAdapter.getBaseAdapter().getAdapterName());
                adapterPortComboBox.getItems().add(genericAdapter.getBaseAdapter().getPortName());
            }
        } catch (OneWireException e) {
            e.printStackTrace();
        }
        evenPortComboBox.getItems().setAll(totalSerialPortList);
        unevenPortComboBox.getItems().setAll(totalSerialPortList);
        checkSensorIdComboBox.getItems().setAll(globalSensorList);
        evenLeftSensorIdComboBox.getItems().setAll(globalSensorList);
        evenRightSensorIdComboBox.getItems().setAll(globalSensorList);
        unevenLeftSensorIdComboBox.getItems().setAll(globalSensorList);
        unevenRightSensorIdComboBox.getItems().setAll(globalSensorList);
        temperatureLeftSensorIdComboBox.getItems().setAll(globalSensorList);
        temperatureRightSensorIdComboBox.getItems().setAll(globalSensorList);
    }

    @FXML
    public void checkSensorStatus(ActionEvent actionEvent) {
        checkSensorStatusByIdFromListThreadService.restart();
    }

    @FXML
    public void checkSensorByIdTextfieldButton(ActionEvent actionEvent) {
        checkSensorStatusByIdThreadService.restart();
    }

    @FXML
    public void saveSensorsIdByMovement(ActionEvent actionEvent) {
        ThermalSettings thermalSettings = (ThermalSettings) thermalSettingsService.get(1);
        if (thermalSettings == null) {
            thermalSettings = new ThermalSettings();
        }
        if (StringUtils.isNotEmpty(evenLeftSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setEvenLeftIronSensorAddress(evenLeftSensorIdComboBox.getSelectionModel().getSelectedItem());
            evenMovement.getLeftIronSensor().setSensorId(thermalSettings.getEvenLeftIronSensorAddress());
        }
        if (StringUtils.isNotEmpty(evenRightSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setEvenRightIronSensorAddress(evenRightSensorIdComboBox.getSelectionModel().getSelectedItem());
            evenMovement.getRightIronSensor().setSensorId(thermalSettings.getEvenRightIronSensorAddress());
        }
        if (StringUtils.isNotEmpty(unevenLeftSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setUnevenLeftIronSensorAddress(unevenLeftSensorIdComboBox.getSelectionModel().getSelectedItem());
            unevenMovement.getLeftIronSensor().setSensorId(thermalSettings.getUnevenLeftIronSensorAddress());
        }
        if (StringUtils.isNotEmpty(unevenRightSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setUnevenRightIronSensorAddress(unevenRightSensorIdComboBox.getSelectionModel().getSelectedItem());
            unevenMovement.getRightIronSensor().setSensorId(thermalSettings.getUnevenRightIronSensorAddress());
        }
        thermalSettingsService.save(thermalSettings);
        loggerInformation.appendText("Адреса датчиков по направлению движения сохранены. \n");
    }

    @FXML
    public void saveSensorIdByOuterTemperature(ActionEvent actionEvent) {
        ThermalSettings thermalSettings = (ThermalSettings) thermalSettingsService.get(1);
        if (thermalSettings == null) {
            thermalSettings = new ThermalSettings();
        }
        if (StringUtils.isNotEmpty(temperatureLeftSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setTemperatureLeftSensorAddress(temperatureLeftSensorIdComboBox.getSelectionModel().getSelectedItem());
            outsideTemperatureSensors.getLeftOutsideTemperatureSensor().setSensorId(thermalSettings.getTemperatureLeftSensorAddress());
        }
        if (StringUtils.isNotEmpty(temperatureRightSensorIdComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setTemperatureRightSensorAddress(temperatureRightSensorIdComboBox.getSelectionModel().getSelectedItem());
            outsideTemperatureSensors.getRightOutsideTemperatureSensor().setSensorId(thermalSettings.getTemperatureRightSensorAddress());
        }
        thermalSettingsService.save(thermalSettings);
        loggerInformation.appendText(Constants.TEMPERATURE_SENSORS_ID_SAVED);
    }

    @FXML
    public void saveControlPortsNumbers(ActionEvent actionEvent) {
        ThermalSettings thermalSettings = (ThermalSettings) thermalSettingsService.get(1);
        if (thermalSettings == null) {
            thermalSettings = new ThermalSettings();
        }
        if (StringUtils.isNotEmpty(evenPortComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setEvenControlPortAddress(evenPortComboBox.getSelectionModel().getSelectedItem());
            evenMovement.setEvenControlPort(new SerialPort(thermalSettings.getEvenControlPortAddress()));
        }
        if (StringUtils.isNotEmpty(unevenPortComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setUnevenControlPortAddress(unevenPortComboBox.getSelectionModel().getSelectedItem());
            unevenMovement.setUnevenControlPort(new SerialPort(thermalSettings.getUnevenControlPortAddress()));
        }
        thermalSettingsService.save(thermalSettings);
        loggerInformation.appendText(Constants.PORTS_ID_SAVED);
    }

    @FXML
    public void updateAll(ActionEvent actionEvent) {
        updateAllDataThreadService.restart();
    }

    @FXML
    public void сleanLogger(ActionEvent actionEvent) {
        loggerInformation.setText("");
    }

    @FXML
    public void saveAdapterAndPort(ActionEvent actionEvent) {
        ThermalSettings thermalSettings = (ThermalSettings) thermalSettingsService.get(1);
        if (thermalSettings == null) {
            thermalSettings = new ThermalSettings();
        }
        if (StringUtils.isNotEmpty(adapterComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setAdapterAddress(adapterComboBox.getSelectionModel().getSelectedItem());
        }
        if (StringUtils.isNotEmpty(adapterPortComboBox.getSelectionModel().getSelectedItem())) {
            thermalSettings.setAdapterPort(adapterPortComboBox.getSelectionModel().getSelectedItem());
        }
        thermalSettingsService.save(thermalSettings);
        loggerInformation.appendText("Адреса адаптера и портов сохранены. \n");
    }
}
