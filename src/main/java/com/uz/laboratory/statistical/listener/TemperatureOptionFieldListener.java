package com.uz.laboratory.statistical.listener;


public interface TemperatureOptionFieldListener {
    void temperatureChangedValidation(String fieldName, Double newValue);
}
