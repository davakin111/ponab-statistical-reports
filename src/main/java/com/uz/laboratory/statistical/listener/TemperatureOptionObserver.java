package com.uz.laboratory.statistical.listener;


import com.uz.laboratory.statistical.bean.option.TemperatureOption;
import jssc.SerialPortException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class TemperatureOptionObserver implements TemperatureOptionFieldListener {
    @Autowired
    private TemperatureOption temperatureOption;

    @Override
    public void temperatureChangedValidation(String fieldName, Double newTemperatureValue) {
        Double leftTemperature = temperatureOption.getLeftTemperature();
        Double rightTemperature = temperatureOption.getRightTemperature();
        if (temperatureOption.isWarmingOn()
                && temperatureOption.getControlPortAddressByMovment().isOpened()) {
            try {
                System.out.println("Проверка температурных условий...");
                if (fieldName.equals("leftTemperature")
                        && leftTemperature != null
                        && StringUtils.isNotEmpty(leftTemperature.toString())) {
                    temperatureOption.getControlPortAddressByMovment().setDTR(newTemperatureValue <= leftTemperature);
                } else if (fieldName.equals("rightTemperature")
                        && rightTemperature != null
                        && StringUtils.isNotEmpty(rightTemperature.toString())) {
                    temperatureOption.getControlPortAddressByMovment().setRTS(newTemperatureValue <= rightTemperature);
                }
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }
}
